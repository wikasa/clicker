#pragma once
#include <memory>
#include "Loger.h"
/**
 * Klasa pobierająca strumień z logera na pole log
 */
class ZLogerem {
protected:
	std::ostream& log;
public:
	ZLogerem() : log(Loger::getStream()) {}
	~ZLogerem() {}
};