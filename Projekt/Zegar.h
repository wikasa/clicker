#pragma once
#include "BazaKlikalnego.h"
#include "ZMagazynem.h"
/**
 * Klasa pochodna klasy BazaKlikalnego
 * Obs�uguje klikni�cia w tle
 */
class Zegar: public BazaKlikalnego {
public:
	/**
	* Konstruktor wywo�uj�cy konstruktor klasy bazowej
	* @param nazwa
	*/
	Zegar() : BazaKlikalnego("Zegar") { }
	/**
	* Metoda obs�uguj�ca ilo�� klikni��
	*/
	void klikniecie() {
		unsigned int mnoznikTlo = magazyn->getMnoznikTlo();
		magazyn->dodajKlikniecia(1 * mnoznikTlo);
		magazyn->dodajDuchy(1 * mnoznikTlo);
		magazyn->dodajPieniadze(1 * mnoznikTlo);
	}
	~Zegar() {}
};