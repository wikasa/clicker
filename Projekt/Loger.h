#pragma once
#include <memory>
#include <fstream>
/**
 * Klasa obs�uguj�ca zapis log�w do pliku, singleton
 */
class Loger {
private:
	std::ofstream plik;
	/**
	 * Konstruktor przechowuj�cy plik
	 */
	Loger() {
		plik.open("log.txt");
		if (!plik.is_open() || !plik.good()) {
			throw "nie mo�na stworzy� pliku";
		}
	}
	/**
	 * Inteligentny wska�nik, kt�ry pozwala na dostep tylko do jednego logera
	 */
	static std::shared_ptr<Loger> dostep() {
		static std::shared_ptr<Loger> wskaznik{ new Loger };
		return wskaznik;
	}
public:
	/**
	 * Usuni�cie domy�lnego konstruktora kopiuj�cego
	 */
	Loger(Loger const&) = delete;
	/**
	 * Usuni�cie domy�lnego operatora przypisania kopiuj�cego
	 */
	Loger& operator=(Loger const&) = delete;
	/**
	 * Funkcja zwracaj�ca strumie� w klasie
	 */
	static std::ostream& getStream() {
		auto loger = Loger::dostep();
		return loger->plik;
	}
	/**
	 * Dekonstruktor, zamkni�cie pliku
	 */
	~Loger() {
		this->plik << "zamknieto";
		this->plik.close();
	}
};