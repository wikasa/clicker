#pragma once
#include "ZMagazynem.h"
#include "ZLogerem.h"

/**
 * Klasa bazowa klasy Duch, Ulepszenie, Zegar
 * Klasa pochodna klasy ZMagazynem, ZLogerem
 * Wymusza na klasach pochodnych metod� klikniecie() i pole nazwa
 */
class BazaKlikalnego: protected ZMagazynem, protected ZLogerem {
protected:
	std::string nazwa;
	/**
	 * Konstruktor wywo�uj�cy konstruktory klas bazowych i ustawia nazw�
	 * @param string zawieraj�cy nazw� 
	 */
	BazaKlikalnego(std::string nazwa) : ZMagazynem(), ZLogerem(), nazwa(nazwa) {}
public:
	/**
	 * Metoda czysto wirtualna
	 */
	virtual void klikniecie() = 0;
	/**
	 * Metoda do pobierania nazwy
	 */
	std::string getNazwa() { 
		return nazwa; 
	}
	~BazaKlikalnego() {}
};

/**
 * Przeci��anie operatora strumieniowego
 * @param strumie� wyj�ciowy
 * @param referencja do obiektu
 */
std::ostream& operator<<(std::ostream& wyjscie, BazaKlikalnego& x){
	return wyjscie << x.getNazwa();
}
