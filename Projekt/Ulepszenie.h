#pragma once
#include "BazaKlikalnego.h"
#include "ZMagazynem.h"
/**
* Klasa pochodna klasy BazaKlikalnego
* Obs�uguje wykupienie ulepsze� i ich skutki
*/
class Ulepszenie : public BazaKlikalnego {
private:
	unsigned int koszt;
	unsigned int dodawanyMnoznik;
	unsigned int dodawanyMnoznikTlo;
	/**
	* Konstruktor wywo�uj�cy konstruktor klasy bazowej
	* @param nazwa
	*/
	Ulepszenie() : BazaKlikalnego("Ulepszenie") { }
public:
	/**
	* Konstruktor wywo�uj�cy konstruktor klasy bazowej z nazw� i tworz�cy ulepszenie
	*/
	Ulepszenie(unsigned int koszt, unsigned int dodawanyMnoznik, unsigned int dodawanyMnoznikTlo, std::string nazwa) : BazaKlikalnego(nazwa), koszt(koszt), dodawanyMnoznik(dodawanyMnoznik), dodawanyMnoznikTlo(dodawanyMnoznikTlo) {}
	/**
	* Metoda obs�uguj�ca dzia�anie ulepsze�
	* Obs�uga wyj�tk�w
	* Zapis do pliku
	*/
	void klikniecie() {
			try {
				this->log << "kliknieto " << *this << std::endl;
				magazyn->wydajPieniadze(koszt);
				magazyn->dodajMnoznik(dodawanyMnoznik);
				magazyn->dodajMnoznikTlo(dodawanyMnoznikTlo);
			}
			catch(std::string err){
				throw err;
			}
	}
	~Ulepszenie() {}
};