#pragma once
#include "ZLogerem.h"
#include <memory>
/**
 * Klasa g��wna, przechowuj�ca najwa�niejsze elementy programu
 * Klasa pochodna klasy ZLogerem
 */
class Magazyn :protected ZLogerem{
private:
	/**
	 * Zmienne przechowuj�ce podstawowe informacje potrzebne do dzia�ania programu
	 */
	unsigned int duchy;
	unsigned int pieniadze;
	unsigned int mnoznik;
	unsigned int mnoznikTlo;
	unsigned int klikniecia;
	/**
	 * Prywatny konstruktor, by nie mo�na by�o stworzy� drugiej instancji klasy
	 * Konstruktor argumentowy, by m�c ustawi� stan pocz�tkowy
	 */
	Magazyn(unsigned int duchy = 0, unsigned int pieniadze = 0, unsigned int mnoznik = 1, unsigned int mnoznikTlo = 1, unsigned int klikniecia = 0) :ZLogerem(), duchy(duchy), pieniadze(pieniadze), mnoznik(mnoznik), mnoznikTlo(mnoznikTlo), klikniecia(klikniecia) {
		this->log << "poczatek gry" << std::endl;
	}
public:
	/**
	 * Zaprzyja�nienie do przeci��enia operatora strumieniowania
	 */
	friend std::ostream& operator<<(std::ostream& wyjscie, Magazyn& x);
	/**
	* Usuni�cie domy�lnego konstruktora kopiuj�cego
	*/
	Magazyn(Magazyn const&) = delete;
	/**
	* Usuni�cie domy�lnego operatora przypisania kopiuj�cego
	*/
	Magazyn& operator=(Magazyn const&) = delete;
	/**
	* Metoda klasy zwracaj�ca zawsze wska�nik na t� sam� instancj�
	* U�yto inteligentnych wska�nik�wm poniewa� zabezpieczaj� one przed wyciekami pami�ci
	* Statyczna, poniewa� nale�y do klasy, a nie do obiektu
	*/
	static std::shared_ptr<Magazyn> dostep() {
		static std::shared_ptr<Magazyn> wskaznik{ new Magazyn };
		return wskaznik;
	}
	/**
	* Metoda zwracaj�ca warto�� z pola prywatnego klasy
	*/
	unsigned int getDuchy() { return this->duchy; }
	/**
	* Metoda zwracaj�ca warto�� z pola prywatnego klasy
	*/
	unsigned int getPieniadze() { return this->pieniadze; }
	/**
	* Metoda zwracaj�ca warto�� z pola prywatnego klasy
	*/
	unsigned int getMnoznik() { return this->mnoznik; }
	/**
	* Metoda zwracaj�ca warto�� z pola prywatnego klasy
	*/
	unsigned int getMnoznikTlo() { return this->mnoznikTlo; }
	/**
	* Metoda zwracaj�ca warto�� z pola prywatnego klasy
	*/
	unsigned int getKlikniecia() { return this->klikniecia; }

	/**
	* Metoda do dodawania i wypisywania w pliku
	* @param dodana warto��
	*/
	void dodajDuchy(unsigned int dodane) {
		duchy += dodane*mnoznik;
		this->log << "dodano duchy: " << dodane <<" "<< *this;
	}
	/**
	* Metoda do dodawania i wypisywania w pliku
	* @param dodana warto��
	*/
	void dodajPieniadze(unsigned int dodane) { 
		pieniadze += dodane; 
		this->log << "dodano pieniadze: " << dodane << " " << *this;
	}
	/**
	* Metoda do dodawania i wypisywania w pliku
	* @param dodana warto��
	*/
	void dodajMnoznik(unsigned int dodane) { 
		mnoznik += dodane; 
		this->log << "dodano mnoznik: " << dodane << " " << *this;
	}
	/**
	* Metoda do dodawania i wypisywania w pliku
	* @param dodana warto��
	*/
	void dodajMnoznikTlo(unsigned int dodane) { 
		mnoznikTlo += dodane; 
		this->log << "dodano mnoznik tlo: " << dodane << " " << *this;
	}
	/**
	* Metoda do dodawania i wypisywania w pliku
	* @param dodana warto��
	*/
	void dodajKlikniecia(unsigned int dodane) { 
		klikniecia += dodane;
		this->log << "dodano klikniecia: " << dodane << " " << *this;
	}
	/**
	* Metoda do resetowania warto�ci zmiennych w programie i w pliku
	*/
	void reset() { 
		duchy = 0;
		pieniadze = 0;
		mnoznik = 1;
		mnoznikTlo = 1;
		klikniecia = 0;
		this->log << "reset: "<< *this;
	}
	/**
	* Metoda do odejmowania i wypisywania w pliku
	* Zaimplementowanie wykrywanie b��du i wpisywanie komunikatu do pliku, obs�uga b��du
	* @param odj�ta warto��
	*/
	void wydajPieniadze(unsigned int odjete) {
		if (pieniadze >= odjete) {
				pieniadze -= odjete;
				this->log << "wydano pieniadze: " << odjete << " " << *this;
		}
		else {
			std::string blad = "niewystarczaj�ca ilo�� pieniedzy";
			this->log << blad << std::endl;
			throw blad;
		}
	}
	~Magazyn() {}
};
/**
* Przeci��anie operatora strumieniowego
* @param strumie�
* @param referencja do instancji klasy
*/
std::ostream& operator<<(std::ostream& wyjscie, Magazyn& x) {
	return wyjscie << "stan magazynu: " << "duchy: " << x.getDuchy() << ", " << "pieniadze: " << x.getPieniadze() << ", " << "mnoznik: " << x.getMnoznik() << ", " << "mnoznik tlo: " << x.getMnoznikTlo() << ", " << "klikniecia: " << x.getKlikniecia() << std::endl;
}