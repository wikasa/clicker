#pragma once
#include "BazaKlikalnego.h"
#include "ZMagazynem.h"
/**
* Klasa pochodna klasy BazaKlikalnego
* Obs�uguje klikni�cia i pieni�dze
*/
class Duch: public BazaKlikalnego {
public:
	/**
	* Konstruktor wywo�uj�cy konstruktor klasy bazowej
	* @param nazwa
	*/
	Duch(): BazaKlikalnego("Duch") { }
	/**
	* Metoda obs�uguj�ca dzia�anie ulepsze�
	* Obs�uga wyj�tk�w
	* Zapis do pliku
	*/
	void klikniecie() {
		this->log << "kliknieto "<<*this<<std::endl;
		unsigned int mnoznik = magazyn->getMnoznik();
		magazyn->dodajKlikniecia(1);
		magazyn->dodajDuchy(1 * mnoznik);
		magazyn->dodajPieniadze(1 * mnoznik);
	}
	~Duch() {}
};