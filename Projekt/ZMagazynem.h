#pragma once
#include <memory>
#include "Magazyn.h"

/**
 * Klasa pobierająca wskaźnik na obiekt i przechowująca go w polu magazyn
 */
class ZMagazynem {
protected:
	std::shared_ptr<Magazyn> magazyn;
public:
	ZMagazynem() : magazyn(Magazyn::dostep()){}
	~ZMagazynem() {}
};