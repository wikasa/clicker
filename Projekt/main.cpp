#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <cmath>
#include <memory>
#include <string>
#include <iostream>
#include "Duch.h"
#include "Ulepszenie.h"
#include "Zegar.h"

using namespace sf;
int main(){
	/**
	 * Zmienne typu const przechowuj�ce koszty ulepsze�
	 */
	const unsigned int kosztUl1=200;
	const unsigned int kosztUl2=300;
	const unsigned int kosztUl3=500;
	/**
	 * Zmienna przechowuj�ca limit czasu, tj. 10 minut
	 */
	unsigned int limitCzasu = 60*10;
	/**
	 * Zmienna pomocnicza do sprawdzania czy nie nast�pi� koniec czasu
	 */
	bool koniecCzasu = false;
	/**
	 * Zmienna odliczaj�ca czas wy�wietlania komunikatu zwi�zanego z b��dem
	 */
	unsigned short ileCzasuBlad = 0;
	/**
	 * Wska�nik na jedyny obiekt klasy Magazyn
	 */
	auto magazyn = Magazyn::dostep();
	/**
	 * Zdefiniowanie zegara, tekstur, sprit�w i tekst�w, a tak�e innych niezb�dnych element�w interfejsu graficznego
	 */
	sf::Clock zegar;
	sf::RenderWindow okno(sf::VideoMode(800, 600, 32), "Clicker");
	sf::Texture tekstura1;
	sf::Texture tekstura2;
	sf::Texture tekstura3;
	sf::Texture tekstura4;
	tekstura1.loadFromFile("duch1.jpg");
	tekstura2.loadFromFile("duch2.jpg");
	tekstura3.loadFromFile("duch3.jpg");
	tekstura4.loadFromFile("duch4.jpg");
	sf::Sprite duch;
	sf::Texture teksturyDuchow[4] = { tekstura1, tekstura2, tekstura3, tekstura4 };
	unsigned short ktoraTekstura = 0;
	unsigned short mod10Klikniec = 0;
	sf::RectangleShape ul1(sf::Vector2f(300, 70));
	sf::RectangleShape ul2(sf::Vector2f(300, 70));
	sf::RectangleShape ul3(sf::Vector2f(300, 70));
	sf::Font font;
	if (!font.loadFromFile("cour.ttf")) {
		exit(-1);
	}
	sf::Text ulepszenie1("Lepsza bron \n klik x5", font, 25);
	sf::Text ulepszenie1koszt(std::to_string(kosztUl1)+"$", font, 25);
	sf::Text ulepszenie2("Pomocnicy \n tlo x3", font, 25);
	sf::Text ulepszenie2koszt(std::to_string(kosztUl2) + "$", font, 25);
	sf::Text ulepszenie3("Uzbrojeni pomocnicy\n klik x2 x2", font, 25);
	sf::Text ulepszenie3koszt(std::to_string(kosztUl3) + "$", font, 25);
	sf::Text opisklikniecia("Klikniecia: \n", font, 25);
	sf::Text klikniecia("0", font, 40);
	sf::Text opispieniadze("Pieniadze: \n", font, 25);
	sf::Text pieniadze("0 $", font, 40);
	sf::Text opismnoznik("Mnoznik: \n", font, 25);
	sf::Text mnoznik("x 1", font, 40);
	/**
	 * Inteligentny wska�nik na obiekt klasy Duch
	 */
	std::shared_ptr<Duch> duchKlasa{ new Duch };
	/**
	* Inteligentny wska�nik na obiekt klasy Zegar
	*/
	std::shared_ptr<Zegar> zegarKlasa{ new Zegar };
	/**
	* Inteligentne wska�niki na trzy r�ne obiekty klasy Ulepszenie
	* @param koszt ulepszenia
	* @param mno�nik
	* @param mnoznik w tle
	* @param nazwa ulepszenia
	*/
	std::shared_ptr<Ulepszenie> ul1Klasa{ new Ulepszenie(kosztUl1, 5, 0, "Ulepszenie 1") };
	std::shared_ptr<Ulepszenie> ul2Klasa{ new Ulepszenie(kosztUl2, 0, 3, "Ulepszenie 2") };
	std::shared_ptr<Ulepszenie> ul3Klasa{ new Ulepszenie(kosztUl3, 2, 2, "Ulepszenie 3") };
	/**
	* Zmienna przechowuj�ca zmierzony czas, w sekundach
	*/
	int lastTime = zegar.getElapsedTime().asSeconds();
	/**
	* P�tla wykonuj�ca program, je�li jest otwarty
	*/
	while (okno.isOpen()){
		/**
		* Zmienna przechowuj�ca zmierzony czas, w sekundach
		*/
		int now = zegar.getElapsedTime().asSeconds();
		/**
		* Sprawdzenie, czy nie min�� czas
		*/
		if (lastTime != now) {
			/**
			* Sprawdzenie, czy nie jest koniec czasu gry, odliczanie limitu i dzia�anie w tle
			*/
			if (!koniecCzasu) { 
				zegarKlasa->klikniecie();
				limitCzasu--;
			}
			/**
			* Wypisywanie na ekranie warto�ci klikni��, pieni�dzy i duch�w
			*/
			klikniecia.setString(std::to_string(magazyn->getKlikniecia()));
			pieniadze.setString(std::to_string(magazyn->getPieniadze())+ "$");
			mnoznik.setString("x"+std::to_string(magazyn->getMnoznik()));
			/**
			* Je�li koniec czasu, to zatrzymanie gry
			*/
			if (limitCzasu <= 0) {
				koniecCzasu = true;
			}
			/**
			* Wy�wietlanie powiadomie� o b��dzie przez zmniejszany czas
			*/
			if (ileCzasuBlad > 0) {
				ileCzasuBlad--;
			}
			/**
			* Aktualna ilo�� sekund jest zapisywana do zmiennej sprzed chwili
			*/
			lastTime = now;
		}
		duch.setTexture(teksturyDuchow[ktoraTekstura]);
		sf::Event zdarzenie;
		/**
		* P�tla obs�uguj�ca wydarzenie, tj.klikni�cia myszy i klawiszy na klawiaturze
		*/
		while(okno.pollEvent(zdarzenie)) {
			/**
			* Zamkni�cie okna po przyci�ni�ciu ESC
			*/
			if (zdarzenie.type == sf::Event::Closed || (zdarzenie.type==sf::Event::KeyPressed && zdarzenie.key.code==sf::Keyboard::Escape)){
				okno.close();
			}
			/**
			* Reset danych po przyci�ni�ciu R
			*/
			if (zdarzenie.type == sf::Event::KeyPressed && zdarzenie.key.code == sf::Keyboard::R) {
				magazyn->reset();
				koniecCzasu = false;
				limitCzasu = 60*10;
			}
			/**
			* Zdarzenie dotycz�ce klikni�� na ducha i ulepsze�
			*/
			if (zdarzenie.type == sf::Event::MouseButtonPressed && zdarzenie.mouseButton.button == sf::Mouse::Left && !koniecCzasu) {
				if (duch.getGlobalBounds().contains(okno.mapPixelToCoords(sf::Mouse::getPosition(okno)))){
					duchKlasa->klikniecie();
					mod10Klikniec++;
					/**
					* Zmiana obrazka ducha
					*/
					if (mod10Klikniec == 10) {
						ktoraTekstura = ktoraTekstura < 3 ? ktoraTekstura + 1 : 0;
						mod10Klikniec = 0;
					}
				}	
				   /**
					* Sprawdzenie klikni�cia na ulepszenie, obs�uga wyj�tk�w
					*/
					if (ul1.getGlobalBounds().contains(okno.mapPixelToCoords(sf::Mouse::getPosition(okno))) || ulepszenie1.getGlobalBounds().contains(okno.mapPixelToCoords(sf::Mouse::getPosition(okno)))) {
						try {
							ul1Klasa->klikniecie();
						}
						catch(std::string _){
							ileCzasuBlad = 3;
						}
					}
					/**
					* Sprawdzenie klikni�cia na ulepszenie, obs�uga wyj�tk�w
					*/
				if (ul2.getGlobalBounds().contains(okno.mapPixelToCoords(sf::Mouse::getPosition(okno))) || ulepszenie2.getGlobalBounds().contains(okno.mapPixelToCoords(sf::Mouse::getPosition(okno)))) {
					try {
						ul2Klasa->klikniecie();
					}
					catch (std::string _) {
						ileCzasuBlad = 3;
					}
				}
				/**
				* Sprawdzenie klikni�cia na ulepszenie, obs�uga wyj�tk�w
				*/
				if (ul3.getGlobalBounds().contains(okno.mapPixelToCoords(sf::Mouse::getPosition(okno))) || ulepszenie3.getGlobalBounds().contains(okno.mapPixelToCoords(sf::Mouse::getPosition(okno)))) {
					try {
						ul3Klasa->klikniecie();
					}
					catch (std::string _) {
						ileCzasuBlad = 3;
					}
				}
			}
		}
		/**
		 * Dopasowanie wielko�ci, ustawianie pozycji i wy�wietlanie interfejsu graficznego
		 */
		okno.clear(sf::Color(255,127,39));
		duch.setPosition(300, 100);
		ul1.setFillColor(sf::Color(0, 0, 0));
		ul2.setFillColor(sf::Color(0, 0, 0));
		ul3.setFillColor(sf::Color(0, 0, 0));
		ul1.setPosition(0, 50);
		ul2.setPosition(0, 250);
		ul3.setPosition(0, 450);
		opisklikniecia.setPosition(600, 50);
		klikniecia.setPosition(600, 100);
		opispieniadze.setPosition(600, 200);
		pieniadze.setPosition(600, 250);
		opismnoznik.setPosition(600, 350);
		mnoznik.setPosition(600, 400);
		ulepszenie1.setPosition(60, 50);
		ulepszenie1koszt.setPosition(110, 120);
		ulepszenie2.setPosition(70, 250);
		ulepszenie2koszt.setPosition(110, 320);
		ulepszenie3.setPosition(5, 450);
		ulepszenie3koszt.setPosition(110, 520);
		okno.draw(duch);
		okno.draw(ul1);
		okno.draw(ul2);
		okno.draw(ul3);
		okno.draw(opisklikniecia);
		okno.draw(klikniecia);
		okno.draw(opispieniadze);
		okno.draw(pieniadze);
		okno.draw(opismnoznik);
		okno.draw(mnoznik);
		okno.draw(ulepszenie1);
		okno.draw(ulepszenie1koszt);
		okno.draw(ulepszenie2);
		okno.draw(ulepszenie2koszt);
		okno.draw(ulepszenie3);
		okno.draw(ulepszenie3koszt);
		/**
		* Wypisanie b��du na ekranie
		*/
		if (ileCzasuBlad>0) {
			sf::Text blad("niewystarczajaca ilosc pieniedzy", font, 25);
			okno.draw(blad);
		}
		/**
		* Wypisanie ko�ca czasu na ekranie
		*/
		if (koniecCzasu) {
			sf::Text koniec("koniec czasu", font, 25);
			okno.draw(koniec);
		}
		okno.display();
	}
	return 0;
}